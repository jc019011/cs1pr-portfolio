#include "common.h"

void createMap(const char *filename) {
	//Initalizes variables used for this function
	char *data, *p;

	int x, y;
	int currentx, currenty, newx, newy;
	int direction;
	//Max number of rooms in the x and y values
	int minx=0, maxy=4, maxx=4;
	int mapIndex;
	//reads the data of the map parsed in
	data = readFile(filename);
	p = data;

	char roomFileName[MAX_FILENAME_LENGTH];
	int downcounter = 0;
	Entity *currententity = stage.entityHead.next;

	//Loads the map recieved into memory
	for (y = 0; y < 42; y++)
	{
		for (x = 0; x < 42; x++)
		{

			int randint = rand() % 6;
			do
			{
				//If the tile is labed with a # it gives it a random tile colour from one to seven
				if (*p == '#') {
					*p = randint + 49;
				}
				sscanf(p, "%d", &stage.map[x][y]);
				p++;
			} while (*p != ' ' && *p != '\n');
		}
	}
	free(data);

	//creates a random starting position for the pathing algorithm
	int randStartingPos = rand_lim(4);

	/*mapRooms is a map of the room types. It is a 4x4 int array. This line sets the start of the pathing algotihm
	an the room type. The room types are as follows. 0-X is a left right room. 1-X is a left right down room.
	2-X is a room with all entrance types. 3-X is a room with left right up entrances. 4 is the spawn room
	*/
	
	stage.mapRooms[0][randStartingPos] = 4;
	//Sets the player spawn. Y is always the same. X depends on where the first room is created
	player->y = 5 * TILE_SIZE;
	player->x = (5 + 10 * randStartingPos) * TILE_SIZE;
	//Sets the new x and y positions of the algorithm
	currentx = randStartingPos;
	currenty = 0;
	//Sets a random direction for the algorithm to move
	direction = rand_lim(7);
	
	while(1){ 
		//This if state ment checks if the direction is left or right. The algorithm is more likely to move left or right than down
		if (direction == 0 || direction == 1 || direction == 2) {
			downcounter = 0;
			currentx++;
			//If the algorithm cannot move anyfurther right it moves down
			if (currentx < maxx) {
				//As this is a left right room it doesnt matter what room is placed here
				stage.mapRooms[currenty][currentx] = rand_lim(4);
				//if the direction it moves is right we dont want it to move back left again
				direction = rand_lim(3);
			}
			else {
				currentx--;
				direction = 6;
			}
		}
		else if (direction == 3 || direction == 4 || direction == 5) {
			downcounter = 0;
			currentx --;
			if (currentx > minx) {
				stage.mapRooms[currenty][currentx] = rand_lim(4);
				direction = rand_lim(3) + 3;
			}
			else {
				currentx++;
				direction = 6;
			}
		}
		//Checks if the direction to move is down
		if (direction == 6) {
			//Adds to the down counter
			downcounter++;
			currenty ++;
			//If it can move down 
			if (currenty < maxy) {
				if (downcounter >= 2) {
					//If the algorithm has moved down twice make suer the room above it has an up and down opening
					stage.mapRooms[currenty - 1][currentx] = 2;
				}
				else {
					//make sre the room above has a down opening
					stage.mapRooms[currenty][currentx] = rand_lim(1)+2;
				}
				if (stage.mapRooms[currenty - 1][currentx] != 4) {
					stage.mapRooms[currenty - 1][currentx] = 2;
				}
				//Sets a new random direction
				direction = rand_lim(7);
			}
			else {
				//If the algorithm cannot move down set the last room to the spawn point and then break 
				stage.mapRooms[currenty - 1][currentx] = 4;
				break;
			}
		}
	}

	//Set the rest of the rooms in the map equal to random rooms
	for (y = 0; y < 4; y++) {
		for (x = 0; x < 4; x++) {
			if (stage.mapRooms[y][x] == 0) {

				stage.mapRooms[y][x] = rand_lim(4);
			}
		}
	}
	

	//This for loop loads each of the map rooms into stage.map. It itterated through each room in mapRooms and writed them into stage.map
	for (int mapRoomY = 0; mapRoomY < 4; mapRoomY++) {
		for (int mapRoomX = 0; mapRoomX < 4; mapRoomX++) {
			//This is the type of room it is loading in. 
			mapIndex = stage.mapRooms[mapRoomY][mapRoomX];
			//This is the variation of the type of room it is loading in
			int randomRoom = rand_lim(3);
			//Checks to make sure the room is not a spawn room
			if (mapIndex != 4 && stage.pizzaTotal < 15) {
				//Changes the pizzas x and y to the center of the current room
				currententity->x = ((mapRoomX * 10) + 5) * TILE_SIZE;
				currententity->y = ((mapRoomY * 10) + 5) * TILE_SIZE;
				currententity = currententity->next;
				//loads the room filepath into roomFileName
				sprintf(roomFileName, "data/%d-%d.dat", mapIndex, randomRoom);
			}
			else {
				sprintf(roomFileName, "data/%d.dat", mapIndex);
			}
			
			
			//Loads the file path into the current room array
			data = readFile(roomFileName);
			p = data;
			int currentRoom[10][10];
			for (y = 0; y < 10; y++)
			{
				for (x = 0; x < 10; x++)
				{
					int randint = rand() % 6;
					do
					{
						if (*p == '~') {
							break;
						}
						if (*p == '#') {
							*p = randint + 49;
						}
						//If the symbol is an @ it has a chance it doenst appear
						if (*p == '@') {
							if (randint < 2) {
								*p = 48;
							}
							else {
								*p = randint + 49;
							}
						}
						sscanf(p, "%d", &currentRoom[x][y]);
						p++;
					} while (*p != ' ' && *p != '\n');
				}
			}

			//replaces the correct tiles in stage.maao with the current rooms tiles
			for (y = 0; y < 10; y++) {
				for (x = 0; x < 10; x++) {
					newx = (mapRoomX * 10) + 1;
					newy = (mapRoomY * 10) + 1;
					stage.map[newx+x][newy+y] = currentRoom[x][y];
				
				}
			}
			
		}
	}
}

